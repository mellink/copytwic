## Automatic Copy The Week in Chess Archive Files
see: https://theweekinchess.com/twic

### Command:
python3 copytwic.py -s _**start**_ -e _**end**_ -p **_path_** -u **_url_** -v **_verbose_** True

### Results
1. Large PGN file with all selected archivefiles in pgn format
2. Alle downloaded zip and pgn files in directory <path>: 
    
    <path>/twic/<date>.pgn
    <path>/twic/zips
    <path>/twic/pgns

### Example
1. python3 copyTWIC.py 
2. python3 copyTWIC.py -s 920 -e 1316 -p /Users/username -u https://www.theweekinchess.com/zips/ -v False

Remark: The archives files are starting form number 920