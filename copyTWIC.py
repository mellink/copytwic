#Install vereisten MacOS
# sudo pip3 install --upgrade pip
# Install python381
# https://www.python.org/downloads/release/python-381/
# kies macOS 64 bit installer
# run /Applications/Python 3.8/Install Certificates command

# pip3 install requests, wget, datetime, zipfile38
# run -> edit_configurations -> Chose python interpreter with requests, wget, datetime, zipfile38 installed

import requests, wget, getopt
import os.path, os, sys
from zipfile import ZipFile
from datetime import datetime, timedelta

def main(argv):
    try:
      _url = 'https://www.theweekinchess.com/zips/'
      _van = 920
      _tot = week_from_03022020(datetime(2020, 1, 24, 0, 0, 0), datetime.now()) + 1316
      _path= "/Users/mellian"
      _verbose = False
      # https://www.theweekinchess.com/zips/twic1000g.zip
      opts, args = getopt.getopt(argv, "hs:e:p:u:v:", ["start=", "end=", "path=", "url=", "verbose"])
    except getopt.GetoptError:
      print ('copy_twic.py -s <start> -e <end> -p <path> -u <url> -v <verbose> False')
      sys.exit(2)
    for opt, arg in opts:
      if opt == '-h':
         print ('copy_twic.py -s <start> -e <end> -p <path> -u <url> -v <verbose> False')
         print ('copy_twic.py -s 920 -e 1316 -p /Users/<name> -u https://www.theweekinchess.com/zips/ -v False')
         sys.exit()
      elif opt in ("-s", "--start"):
          if arg.isnumeric():
             _van = int(arg)
             if (_van < 920): _van = 920
          else:
             _van = 920
      elif opt in ("-e", "--end"):
          if arg.isnumeric():
             _tot = int(arg)
          else:
             _tot = read_lastfilenr_from_file(_verbose)
      elif opt in ("-p", "--path"):
           _path = arg
      elif opt in ("-u", "--url"):
           _url = arg
      elif opt in ("-v", "--verbose"):
           _verbose = arg
    run_main(_van, _tot+1, _path, _url, _verbose)

def read_lastfilenr_from_file(verbose):
    f = open("Twic_lastfile_nr.txt", "r")
    arg = f.readline()
    f.close()
    if verbose: print(arg)
    if arg.isnumeric():
        return int(arg)
    else:
        return 1316

def write_lastfilenr_from_file(lastfilenr, verbose):
    f = open("Twic_lastfile_nr.txt", "w")
    f.writelines(str(lastfilenr))
    f.close()

def copy_url_file(filename, url, localpath):
    urltot = url+"/"+filename
    print(urltot)
    myfile = requests.get(urltot)
    localname = localpath+name
    print(myfile)
    open(localname, 'wb').write(myfile.content)

def get_url_range(van,tot,url, localpath):
    for i in range(van, tot):
        name = 'twic'+str(i)+'g.zip'
        print(name)
        copy_url_file(name, url, localpath)

def wget_url_range(van, tot, url, localpathzips, detailinfo):
    print("Download TWIC files: "+str(van)+"-"+str(tot))
    for i in range(van, tot):
        name = 'twic'+str(i)+'g.zip'
        urltot = url+name
        localname = localpathzips+name
        if os.path.isfile(localname):
           if detailinfo:
              print("Bestand al gedownload:", localname)
        else:
           print("Bestand ", urltot, " wordt gedownload")
           try:
                wget.download(urltot, localname)
           except:
               if detailinfo: print("Filenr "+ str(i) + "not available")
               write_lastfilenr_from_file(i-1, detailinfo)
               print("Last Filenr:" + str(i-1))
               return i-1
               break
    print("Last Filenr:" + str(tot-1))
    return tot

def unzip_twic_range(van, tot, url, localpathzips, localpathpgns, detailinfo):
    print("Unzip TWIC files")
    for i in range(van, tot):
        namezip = 'twic'+str(i)+'g.zip'
        namepgn = 'twic' + str(i) + '.pgn'
        localnamezip = localpathzips+namezip
        localnamepgn = localpathpgns+namepgn
        if os.path.isfile(localnamepgn):
           if detailinfo:
              print("Bestand al unzipped: ", localnamepgn)
        else:
           print("Bestand ", localnamepgn, " wordt unzipped")
           # Create a ZipFile Object and load sample.zip in it
           with ZipFile(localnamezip, 'r') as zipObj:
                # Extract all the contents of zip file in different directory
                zipObj.extractall(localpathpgns)

def nowdate_string():
    now = datetime.now()  # current date and time
    year = now.strftime("%Y")
    month = now.strftime("%m")
    day = now.strftime("%d")
    return year+month+day

def week_from_03022020(d1, d2):
    monday1 = (d1 - timedelta(days=d1.weekday()))
    monday2 = (d2 - timedelta(days=d2.weekday()))
    weeks = int((monday2 - monday1).days / 7)
    return(weeks)

def concat_twic_pgn_files(localpathTWIC, localpathpgns, detailinfo):
    print("Concat Twic Pgn files")
    daystring = nowdate_string()
    twicfilename = localpathTWIC + "TWIC_" + daystring + ".pgn"
    pgnfiles = []
    # r=root, d=directories, f = files
    for r, d, f in os.walk(localpathpgns):
        for file in f:
            if '.pgn' in file:
                pgnfiles.append(os.path.join(r, file))
    fh = open(twicfilename, "wb")
    if detailinfo: print(pgnfiles)
    for _file in pgnfiles:
        with open(_file, 'rb') as file:
             lines = file.read()
             fh.write(lines)
    return twicfilename

def run_main(van, tot, path, url, _verbose):
    localpathTWIC = path+'/twic/'
    localpathzips = path+'/twic/zips/'
    localpathpgns = path+'/twic/pgns/'
    if not os.path.exists(localpathTWIC):
       os.makedirs(localpathTWIC)
    if not os.path.exists(localpathzips):
       os.makedirs(localpathzips)
    if not os.path.exists(localpathpgns):
       os.makedirs(localpathpgns)

    if _verbose: print(localpathTWIC, localpathzips, localpathpgns)
    tot = wget_url_range(van, tot, url, localpathzips, _verbose)
    unzip_twic_range(van, tot, url, localpathzips, localpathpgns, _verbose)
    twicfn = concat_twic_pgn_files(localpathTWIC, localpathpgns, _verbose)
    print("File created: "+twicfn)

if __name__ == "__main__":
   main(sys.argv[1:])